package com.example.tableapp.entities.configuration

import com.example.tableapp.R

interface ConfigurationApp {

    fun gih(){
        Unit
    }

    class FragmentTable: ConfigurationApp {

        enum class TypeBox{
            EMPTY, // пустой( обычный edit text)
            BLACK, // всмысле как пустая только черного цвета
            TITLE, // как название столбца
        } // форматы ячейки

        enum class TypeLine{
            TITLE,
            CHARACTER
        } // форматы записи( линии ) таблицы

        data class DataBox(
            var numberBox:Int? = null,
            var point:Int = 0,
        ) // формат данных 1 ячейки в таблице

        data class DataLine(
            var numberLine:Int,
            var listBox:List<DataBox> = listOf(),
            var place: Int? = null
        ) // формат данных 1 линии в таблице

        val listConfigForTable: Map<Int,TypeLine> = mapOf(
            R.string.configuration_table_line_zero to TypeLine.TITLE,
            R.string.configuration_table_line_any to TypeLine.CHARACTER,
        )  // тип и имя записи в таблицы (формат заполнения)

    } // Конфигурация фрагмента таблицы

}
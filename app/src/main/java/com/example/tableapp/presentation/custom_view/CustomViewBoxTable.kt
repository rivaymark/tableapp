package com.example.tableapp.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.core.text.isDigitsOnly
import androidx.core.widget.doOnTextChanged
import com.example.tableapp.R
import com.example.tableapp.databinding.CustomViewBoxTableBinding
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.DataBox
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class CustomViewBoxTable @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewBoxTableBinding
    private val job = SupervisorJob()
    private val scope = CoroutineScope( job + Dispatchers.Default)

    private val _dataBoxFlow = MutableStateFlow(DEFAULT)
    val dataBoxFlow = _dataBoxFlow.asStateFlow()

    init {
        val root = inflate(context, R.layout.custom_view_box_table, this)
        binding = CustomViewBoxTableBinding.bind(root)
        setListener()
    }

    fun setColorBox(color:Int = R.color.black) {binding.customViewBoxTextError.setBackgroundResource(color)}
    fun removeEditText() { binding.customViewBoxLayout.visibility = View.GONE }
    fun fillContentAsTitle(number:Int) = binding.apply{
        customViewBoxTextError.text = number.toString()
        removeEditText()
    }

    private fun setListener() = binding.customViewBoxEdit.doOnTextChanged{ text, _, _, _ ->
            scope.launch(Dispatchers.Main) {
                var number  = DEFAULT
                when{
                    text.isNullOrEmpty()-> {
                        setColorBox(com.google.android.material.R.color.mtrl_btn_transparent_bg_color)
                    }
                    text.isDigitsOnly() && text.toString().toInt() in GOOD_RANGE-> {
                        setColorBox(R.color.green)
                        number = text.toString().toInt()
                    }
                    text.isDigitsOnly() && text.toString().toInt() in BAD_RANGE -> {
                        setColorBox(R.color.red)
                        message()
                    }
                    else -> {
                        message()
                    }
                }
                _dataBoxFlow.emit(number)
                job.complete()
                job.join()
            }
        }
    private fun message() = Snackbar.make(binding.root,"БУ!",Snackbar.LENGTH_SHORT).show()

    companion object{
        const val DEFAULT = 0
        val BAD_RANGE = 6..9
        val GOOD_RANGE = 0..5
    }
}
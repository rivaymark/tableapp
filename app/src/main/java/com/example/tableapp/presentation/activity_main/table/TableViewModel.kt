package com.example.tableapp.presentation.activity_main.table

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tableapp.entities.configuration.ConfigurationApp
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.DataLine
import com.example.tableapp.presentation.adapters.AdapterGroupieTableLine
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class TableViewModel @Inject constructor() : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope( job + viewModelScope.coroutineContext + Dispatchers.IO)

    // количество участников
    private val _currentNumberMember = MutableStateFlow<Int>(START_VALUE)
    val currentNumberMember = _currentNumberMember.asStateFlow()

    // Данные об участниках
    private val _dataOfMember = MutableStateFlow<List<DataLine>>(emptyList())
    val dataOfMember = _dataOfMember.asStateFlow()

    // отдать данные об участниках
    private val _giveBackDataOfMember = MutableSharedFlow<List<DataLine>>()
    val giveBackDataOfMember = _giveBackDataOfMember.asSharedFlow()

    // Линии
    private val _dataLine = MutableStateFlow<List<AdapterGroupieTableLine>>(emptyList())
    val dataLine = _dataLine.asStateFlow()

    fun whoIsWin(list: List<DataLine>){
        scope.launch {
            var tempList = list
            // сортировать по количеству набранных очков
            tempList = tempList.sortedByDescending { totalPoints(it.listBox) }
            tempList.forEachIndexed { index, dataLine ->
                // индекс равен тому месту который заняли
                dataLine.place = index + 1
            }
            _giveBackDataOfMember.emit(tempList)
            _dataOfMember.emit(tempList)
            job.complete()
            job.join()
        }
    }

    fun addNewMember(number:Int) = scope.launch {
        _currentNumberMember.emit(number)
        job.complete()
        job.join()
    }

    fun addNewLine(list:List<AdapterGroupieTableLine>) = scope.launch {
        _dataLine.emit(list)
        job.complete()
        job.join()
    }

    private fun totalPoints(box:List<ConfigurationApp.FragmentTable.DataBox>):Int{
        var temp: Int = 0
        box.forEach { temp += it.point }
        return  temp
    }

    companion object{
        const val START_VALUE = 7
    }
}
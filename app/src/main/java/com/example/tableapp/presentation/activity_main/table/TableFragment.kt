package com.example.tableapp.presentation.activity_main.table

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tableapp.databinding.FragmentTableBinding
import com.example.tableapp.entities.configuration.ConfigurationApp
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.TypeLine
import com.example.tableapp.presentation.adapters.AdapterGroupieTableLine
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class TableFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: TableViewModelFactory
    private val viewModel: TableViewModel by viewModels { viewModelFactory }

    private val job = SupervisorJob()
    private val scopeDef = CoroutineScope( job + lifecycleScope.coroutineContext + Dispatchers.Default)
    private val scopeMain = CoroutineScope( job + lifecycleScope.coroutineContext + Dispatchers.Main)

    private var _binding: FragmentTableBinding? = null
    private val binding get() = _binding!!
    private var adapterRv: GroupieAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTableBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
          hideKeyboardFrom()
          initAdapter()
          clickButton()
          viewModel.addNewLine( createListLine(viewModel.currentNumberMember.value)) // старт

          viewModel.dataLine.onEach {
              updateTable(it)
          }.launchIn(scopeDef)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun initAdapter(){
        adapterRv = GroupieAdapter()
        binding.fragmentTableRv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterRv
        }
    }  // инициализация адаптера

    private fun clickButton() {
        binding.apply {
            fragmentTableButtonAdd.setOnClickListener {
                addLine()
            } // Добавить новый элемент
            fragmentTableButtonDelete.setOnClickListener {
                deleteLine()
            } // Удалить все ( количество элементов равно 0)
        }
    }  // установка слушателей кнопок

    private fun createListLine(number:Int,currentNumber:Int = adapterRv!!.itemCount): List<AdapterGroupieTableLine>{
        val tempList = mutableListOf<AdapterGroupieTableLine>()
        if (number < currentNumber) return tempList.toList()
        for (i in currentNumber..number){
            val item = if ( i == 0 ) { createLine(i,TypeLine.TITLE) }
            else  createLine(i,TypeLine.CHARACTER) { flow->
                flow.onEach {
                    Log.d("aaa","подписка линии вызвалась")
                    viewModel.whoIsWin(checkLine(it))
                }.launchIn(scopeDef)
            }
            tempList.add(item)

        }
        return tempList.toList()
    }

    private fun createLine(
        numberLine:Int,
        typeLine:TypeLine,
        giveBackFlowDataLine: (Flow<ConfigurationApp.FragmentTable.DataLine>) -> Unit = {}
    ) = AdapterGroupieTableLine(
            numberLine,
            typeLine,
            viewModel.currentNumberMember.map { it },
            viewModel.giveBackDataOfMember.map {
                Log.d("aaa","тебя тут не будет ${it}")
                it
            })
    { flow -> giveBackFlowDataLine(flow)}

    private fun checkLine(data: ConfigurationApp.FragmentTable.DataLine):List<ConfigurationApp.FragmentTable.DataLine> {
        val list = viewModel.dataOfMember.value.toMutableList()
        var check = true
        //если есть такая линия обновить данные
        list.forEach {
            if (it.numberLine == data.numberLine) {
                it.listBox = data.listBox.toList()
                it.place = data.place
                check = false
            }
        }
        // если такой линии нет добавить
        if (check) list.add(data)
        return list.toList()
    }

    private fun hideKeyboardFrom() {
        val imm = this.context?.applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    } // для фрагмента( убрать клавиатуру)

    private fun deleteLine(){
        scopeDef.launch {
            if (viewModel.currentNumberMember.value == 0){/*сообщение мб*/ }
            else {
                val value = viewModel.currentNumberMember.value
                viewModel.addNewMember(value - 1)
                if (adapterRv!!.itemCount > 0) {
                    val list = scopeDef.async {
                        val temp = viewModel.dataLine.value.toMutableList()
                        temp.remove(temp[value])
                        temp.toList()
                    }
                    viewModel.addNewLine(list.await())
                }
            }
            job.complete()
            job.join()
        }
    }

    private fun addLine(){
        val value =  viewModel.currentNumberMember.value
        viewModel.addNewMember(value + 1)
        val item = createLine(value + 1,TypeLine.CHARACTER)
        val list = viewModel.dataLine.value.toMutableList()
        list.add(item)
        viewModel.addNewLine(list.toList())
    }

    private fun updateTable(items:List<AdapterGroupieTableLine>){
        if (items.isEmpty()){} else {
            scopeMain.launch(Dispatchers.Main) {
                adapterRv!!.updateAsync(items)
                job.complete()
                job.join()
            }
        }
    }
}
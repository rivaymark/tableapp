package com.example.tableapp.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.ScrollView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.example.tableapp.R
import com.example.tableapp.databinding.CustomViewLineTableBinding
import com.example.tableapp.presentation.adapters.AdapterGroupieTableBox
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.viewbinding.BindableItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CustomViewLineTable  @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewLineTableBinding
    private var adapterRv: GroupieAdapter? = null
    private val job = SupervisorJob()
    private val scope = CoroutineScope( job + Dispatchers.Main)

    init {
        val root = inflate(context, R.layout.custom_view_line_table, this)
        binding = CustomViewLineTableBinding.bind(root)
    }

    /*****************************************************************/
    /************************       контент        *******************/
    /*****************************************************************/

    fun initAdapter(){
        adapterRv = GroupieAdapter()
        binding.customViewLineTableRv.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterRv
        }
    }

    fun addNewBox(items: List<AdapterGroupieTableBox>) {
        scope.launch {
            adapterRv!!.updateAsync(items)
            job.complete()
            job.join()
        }
    }

    fun getNumberItems() = adapterRv?.itemCount ?: 0
    fun changeTotalPoints(points:Int){ binding.customViewLineTableTotalPoints.text  = points.toString() }
    fun changePlace(place:Int?){ binding.customViewLineTablePlace.text = if(place == null) context.applicationContext.getString(R.string.text_for_view_hint) else place.toString() }

    /*****************************************************************/
    /************************  Конфигурация линии  *******************/
    /*****************************************************************/

    fun fillContentAsTitle() = binding.apply{
        customViewLineTableMemberNumber.text = context.applicationContext.getString(R.string.text_for_view_number_of)
        customViewLineTableMemberTitle.text = context.applicationContext.getString(R.string.text_title_for_table_name)
        customViewLineTablePlace.text = context.applicationContext.getString(R.string.text_title_for_table_place)
        customViewLineTableTotalPoints.text = context.applicationContext.getString(R.string.text_title_for_table_total_points)
        //конвертировать в sp размер текста
        customViewLineTableTotalPoints.textSize = context.applicationContext.resources.getDimension(R.dimen.for_text_middle_size)  / resources.displayMetrics.scaledDensity
    }

    fun fillContentAsMember(number:Int) = binding.apply{
        customViewLineTableMemberNumber.text = number.toString()
        customViewLineTableMemberTitle.text = context.applicationContext.getString(R.string.text_title_for_table_member)
        customViewLineTablePlace.text = context.applicationContext.getString(R.string.text_for_view_hint)
        customViewLineTableTotalPoints.text = context.applicationContext.getString(R.string.text_for_view_hint)
    }
}
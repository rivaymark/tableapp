package com.example.tableapp.presentation.activity_main.table

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class TableViewModelFactory @Inject constructor(
    private val viewModel: TableViewModel
): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom(TableViewModel::class.java)){
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}
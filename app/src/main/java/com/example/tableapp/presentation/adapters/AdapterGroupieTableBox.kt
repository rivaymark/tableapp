package com.example.tableapp.presentation.adapters

import android.util.Log
import android.view.View
import com.example.tableapp.R
import com.example.tableapp.databinding.SampleRvBoxBinding
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.DataBox
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.TypeBox
import com.xwray.groupie.viewbinding.BindableItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class AdapterGroupieTableBox(
    private val typeBox: TypeBox,                                        // тип ячейки
    private val number :Int,                                             // номер ячейки
    private val giveBackFlowBoxValue: (Flow<DataBox>)-> Unit = {Unit},   // отдать значение ячейки( если ошибка отдаст 0
): BindableItem<SampleRvBoxBinding>() {

    override fun initializeViewBinding(view: View)  = SampleRvBoxBinding.bind(view)

    override fun getLayout(): Int = LAYOUT

    override fun bind(viewBinding: SampleRvBoxBinding, position: Int) {
        viewBinding.root.apply {
            when(typeBox){
                TypeBox.EMPTY -> {
                    giveBackFlowBoxValue(getFlow(dataBoxFlow))
                }
                TypeBox.BLACK -> {
                    removeEditText()
                    setColorBox()
                }
                TypeBox.TITLE -> fillContentAsTitle(number)
            }
        }
    }

    private fun getFlow(flow:Flow<Int>):Flow<DataBox> = flow.map { value ->
            Log.d("aaa", "AdapterGroupieTableBox. отдаю данные ячейки $value")
            DataBox(
                numberBox = number,
                point = value
            )
        }.stateIn(
            scope = CoroutineScope(Dispatchers.Default),
            started = SharingStarted.WhileSubscribed(DELAY),
            initialValue = DataBox(numberBox = number)
        )

    companion object {
        const val DELAY = 100L
        const val LAYOUT = R.layout.sample_rv_box
    }

}
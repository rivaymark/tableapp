package com.example.tableapp.presentation.adapters

import android.util.Log
import android.view.View
import com.example.tableapp.R
import com.example.tableapp.databinding.SampleRvLineBinding
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.TypeBox
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.DataBox
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.DataLine
import com.example.tableapp.entities.configuration.ConfigurationApp.FragmentTable.TypeLine
import com.xwray.groupie.viewbinding.BindableItem
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class AdapterGroupieTableLine(
    private val numberLine: Int,                                        // номер линии
    private val typeLine: TypeLine,                                     // тип линии
    private val getFlowItems: Flow<Int>,                                // подписка на количество ячеек
    private val getFlowPlace: Flow<List<DataLine>>,                     // подписка на место ( значение места )
    private val giveBackFlowDataLine: (Flow<DataLine>)-> Unit           // отдать значение
): BindableItem<SampleRvLineBinding>() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope( job + Dispatchers.Default)

    // Данные ячеек
    private val _flowBoxData = MutableStateFlow<List<DataBox>>( emptyList())
    private val flowBoxData = _flowBoxData.asStateFlow()

    // Ячейки
    private val _flowBoxItems = MutableStateFlow<List<AdapterGroupieTableBox>>( emptyList())
    private val flowBoxItems = _flowBoxItems.asStateFlow()

    // Данные линии
    private val _flowDataLine = MutableStateFlow<DataLine>(DataLine(numberLine))
    private val flowDataLine = _flowDataLine.asStateFlow()

    override fun initializeViewBinding(view: View)  = SampleRvLineBinding.bind(view)

    override fun getLayout(): Int = LAYOUT

    override fun bind(viewBinding: SampleRvLineBinding, position: Int) {
        viewBinding.root.apply {
            //инициализация адаптера
            initAdapter()
            // отправить стартовое значение линии
            changeLine()
            // определение типа линии
            when(typeLine){
                TypeLine.TITLE -> {
                    //заполнить как заголовок
                    fillContentAsTitle()
                }
                TypeLine.CHARACTER -> {
                   //заполнить как участника
                    fillContentAsMember(numberLine)
                }
            }
            // количество ячеек
            getFlowItems.onEach {
                    val tempList = createListItem(it,getNumberItems())
                    // число меньше текущего вернули пустой список - значит хотим удалить
                    if (tempList.isEmpty() && flowBoxItems.value.isNotEmpty()){
                        try {
                            val tempListForRemove = flowBoxItems.value.toMutableList()
                            tempListForRemove.remove(tempListForRemove[it])
                            _flowBoxItems.emit(tempListForRemove.toList())
                        }
                        catch (e:Exception){ Log.d("aaa","getFlowItems. $e")}
                    }
                    else {
                        _flowBoxItems.emit(tempList)
                }
            }.launchIn( scope )

            //подписка на данные после определения места
            getFlowPlace.onEach { list->
                Log.e("aaa","whoIsWin распределяем места $list")
                if (typeLine == TypeLine.TITLE){ }
                else{
                    list.forEach { dataLine->
                        if (dataLine.numberLine == numberLine){
                            //todo косяк подумать как исправить (на старте)
                            try {
                                changePlace(dataLine.place )             // занятое место
                                changeTotalPoints(totalPoints(dataLine)) // сумма очков
                            }
                            catch (e: Exception){ Log.d("aaa","getFlowPlace.onEach $e") }
                        }
                    }
                }

            }.launchIn( scope )

            //Обновить количество ячеек или добавить
            flowBoxItems.onEach {
                addNewBox(it)
            }.launchIn(CoroutineScope( Dispatchers.Main ))
        }
    }

    private fun createListItem(numberNew: Int, numberCurrent:Int, typeLine: TypeLine = this.typeLine):List<AdapterGroupieTableBox>{
        val tempList = mutableListOf<AdapterGroupieTableBox>()
        if (numberNew < numberCurrent)  return tempList.toList()
        for(i in (1) ..numberNew){
            when(typeLine){
                TypeLine.TITLE -> { tempList.add(createBox(TypeBox.TITLE,i)) }
                TypeLine.CHARACTER -> {
                    if (numberLine == i) tempList.add(createBox(TypeBox.BLACK,i))
                    else {
                        tempList.add(createBox(TypeBox.EMPTY,i){ flowData->
                            flowData.onEach {
                                Log.d("aaa"," createListItem.подписка  $it")
                                val task = scope.async {
                                    changeData(it)  // данные ячейки изменились
                                    true
                                }
                                if(task.await()) changeLine() // значит и данные о линии в том числе
                            }.launchIn(scope) }
                        )
                    }
                }
            }
        }
        return tempList.toList()
    }

    private fun createBox(
        typeBox: TypeBox,
        number :Int,
        giveBackFlowBoxValue: (Flow<DataBox>)-> Unit = {Unit},
    ) = AdapterGroupieTableBox(typeBox, number, giveBackFlowBoxValue) //создать ячейку

    private fun changeData(dataBox: DataBox) {
        scope.launch {
            var result: Boolean = true
            val tempList = flowBoxData.value.toMutableList()
            //есть ли уже такая ячейка
            tempList.forEach {
                if (dataBox.numberBox == it.numberBox) {
                    it.point = dataBox.point
                    result = false
                }
            }
            // если нет добавить
            if (result){ tempList.add(dataBox) }
            Log.d("aaa"," я изменил даннные $tempList")
            _flowBoxData.emit(tempList)
            job.complete()
            job.join()
        }
    } //Добавить новую ячейку или изменить ее данные

    private fun changeLine(){
        if (typeLine == TypeLine.CHARACTER){
            scope.launch {
                // обновить данные о линии
                val task = async {
                    _flowDataLine.emit(
                        DataLine(
                            numberLine = numberLine,
                            listBox = flowBoxData.value.toList()
                        )
                    )
                    true
                }
                //  сообщить об изменениях на вверх ( отдать новые данные
                if (task.await()) {
                    Log.d("aaa", " я изменил линию ${flowDataLine.value}")
                    giveBackFlowDataLine(flowDataLine)
                }
                job.complete()
                job.join()
            }
        }
    }
    //обновить данные линии

    private fun totalPoints(data:DataLine):Int{
        var temp: Int = 0
        data.listBox.forEach { temp += it.point }
        return temp
    } ///подсчитать количетсво очков

    companion object {
        const val LAYOUT = R.layout.sample_rv_line
    }

}